"""
ASGI config for django_websocket project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
#导入配置支持websocket和http
from channels.routing import ProtocolTypeRouter,URLRouter
#导入chat应用的路由模块
from chat import routings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_websocket.settings')

# application = get_asgi_application()
#3、修改asgi启动，支持http也支持websocket
application = ProtocolTypeRouter({
    'http':get_asgi_application(),
    'websocket':URLRouter(routings.socket_urlpatterns) #导入websocket的路由
})